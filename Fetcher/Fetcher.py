from discord.ext import commands
from redbot.core import Config
import aiohttp
import json

class Fetcher(object):

    def __init__(self):
        self.config = Config.get_conf(self, identifier=2042170109130308010909170902)


    @commands.group(pass_context=True)
    async def verify(self,ctx):
        '''Adds a given User to his teams' role and removes him from unverified role'''
        if ctx.invoked_subcommand is None:
            await self.bot.send_cmd_help(ctx)

    @verify.command(pass_context=True)
    async def blue(self,ctx,user : discord.Member):
        '''Assigns the Mystic role to a User'''
        if self.config.teams.blue is not None:
            await self.bot.add_roles(user,self.config.teams.blue)
            await self.bot.remove_roles(user,self.config.teams.unverified)
            await self.bot.say(f"<:checkmark2:373402818364702731> {user.mention} is now a member of Team Mystic.")

    @verify.command(pass_context=True)
    async def red(self,ctx,user : discord.Member):
        '''Assigns the Valor role to a User'''
        if self.config.teams.blue is not None:
            await self.bot.add_roles(user,self.config.teams.red)
            await self.bot.remove_roles(user,self.config.teams.unverified)
            await self.bot.say(f"<:checkmark2:373402818364702731> {user.mention} is now a member of Team Valor.")

    @verify.command(pass_context=True)
    async def yellow(self,ctx,user : discord.Member):
        '''Assigns the Instinct role to a User'''
        if self.config.teams.blue is not None:
            await self.bot.add_roles(user,self.config.guild(ctx.guild).yellow())
            await self.bot.remove_roles(user,self.config.guild(ctx.guild).unverified)
            await self.bot.say(f"<:checkmark2:373402818364702731> {user.mention} is now a member of Team Instinct.")

    @commands.command(pass_context=True)
    async def get_roles(self):
        blue_roles = ('Team Mystic','Mystic','Weisheit','Team Weisheit','Mystic - Weisheit','Weisheit - Mystic')
        red_roles = ('Team Valor','Valor','Wagemut','Team Wagemut','Valor - Wagemut','Weisheit - Valor')
        yellow_roles = ('Team Instinct','Instinct','Team Intuition','Instinct - Intuition','Intuition - Instinct')
        grey_roles = ('Undecided','Uninitiated','Unentschieden')
        unverified_roles = ('Unverified','Unverifiziert','Nicht verifiziert','Neuling')
        await self.bot.say('Okay. Looking up roles....')
        for role in ctx.message.server.roles:
                if role.name in blue_roles:
                    self.config.register_guild({"teams":{"blue": role}})
                    await self.bot.say(f"Found role({role.id}) for Team Mystic")
                elif role.name is in red_roles:
                    self.config.register_guild({"teams":{"red": role}})
                    await self.bot.say(f"Found role({role.id}) for Team Valor")
                elif role.name is in yellow_roles:
                    self.config.register_guild({"teams":{"yellow": role}})
                    await self.bot.say(f"Found role({role.id}) for Team Instinct")
                elif role.name is in grey_roles:
                    self.config.register_guild({"teams":{"grey": role}})
                    await self.bot.say(f"Found role (ID:{role.id}) for undecided players.")
                elif role.name is in unverified_roles:
                    await self.bot.say(f"Found role({role.id}) for Unverified accounts")
                    self.config.register_guild({"teams":{"unverified": role}})

    @command.group(pass_context=True)
    async def scraper(self,ctx):
        '''Enables the Scraper, sets various neccessary settings and starts the system'''
        if ctx.invoked_subcommand is None:
            await self.bot.send_cmd_help(ctx)

    @scraper.command(pass_context=True)
    async def help(self,ctx):
        await self.bot.send_cmd_help(ctx)

